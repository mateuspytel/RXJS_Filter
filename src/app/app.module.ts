import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ObservableComponent } from './observable/observable.component';
import { DataService } from './data.service';
import { Ng2Webstorage } from 'ngx-webstorage';
import { FormComponent } from './form/form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TestingComponent } from './testing/testing.component';
import { DrawerComponent } from './drawer/drawer.component';
import { GridLayoutComponent } from './grid-layout/grid-layout.component';
import { CompbyclassComponent } from './ui/compbyclass/compbyclass.component';


@NgModule({
  declarations: [
    AppComponent,
    ObservableComponent,
    FormComponent,
    TestingComponent,
    DrawerComponent,
    GridLayoutComponent,
    CompbyclassComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    Ng2Webstorage
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
