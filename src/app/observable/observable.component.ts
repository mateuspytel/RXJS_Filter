import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService, Data } from '../data.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { tap, skip, takeUntil, scan, switchMap, mergeMap, shareReplay, take, share, concatMap  } from 'rxjs/operators';
import { merge } from 'rxjs/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/range';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/count';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/take';
import { Subscription } from 'rxjs/Subscription';
import { takeWhile } from 'rxjs/operators';
import { filter } from 'rxjs/operator/filter';


@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.css']
})
export class ObservableComponent implements OnInit, OnDestroy {

  _data: Array<Data> = [];
  _dataSubscription: Subscription;
  _keyEvent$: Subject<any> = new Subject();
  _reset$: Subject<void> = new Subject();

  _execTime = {
    start: 0,
    end: 0
  };

  // Current observable stream
  _dataState$: Observable<any>;

  _page = {
    currentPage: 0,
    pageLimit: 20,
    sortBy: null
  };

  sortByProperty(property: string) {
    this._page.sortBy = property;
    this.getNextPage();
  }

  changePage(dir: number) {

    // Change direction
    switch (dir) {
      case -1: {
        if (this._page.currentPage > 0) {
          this._page.currentPage--;
          this.getNextPage();
        }
        break;
      }
      case 1: {
          this._page.currentPage++;
          this.getNextPage();
        break;
      }
    }
  }


  updateTable(soruce$: Observable<any>): void {

    // Update data in table by subscribing
    // to the Observable passed as a parameter
    soruce$
    .subscribe(
      row => {
        this._data.push(row);
      },
      err => {},
      () => {
        this._execTime.end = new Date().getTime();
        console.warn('Update complete');
        console.warn('Overall update time: ', (this._execTime.end - this._execTime.start), ' ms' );
      }
    );
  }


  getNextPage(): void {

    // Reset array
    this._data = [];
    this._execTime.start = new Date().getTime();

    // Get observable data
    const preSortedStream$ =

      // Accumulate stream data into an array
      this.dataService.getDataObservable().toArray()
      .map( items => {

        // Sort array if sort property is defined
        // if no sorting options are defined
        // - pass original stream without sorting
        if (this._page.sortBy ) {
          items.sort(this.dataService.compareByPropertyName(this._page.sortBy, 1));
          return items;
        } else {
          return items;
        }
      }

      // Turn sorted array back into an observable
    ).pipe( mergeMap( _ => Observable.from(_) ) );

    // Save sorted stream state
    this._dataState$ = preSortedStream$;

    // Paginate sorting results
    const finalSortedStream$ = preSortedStream$.pipe( skip(this._page.currentPage * this._page.pageLimit), take(this._page.pageLimit) );
    this.updateTable(finalSortedStream$);

  }

  filterTable(e: any) {

    // Emit key stroke event
    this._execTime.start = new Date().getTime();
    this._keyEvent$.next(e);

    const filter$ = this._keyEvent$
    .filter( (event: any) => [ 16, 20, 37, 38, 39, 40, 46 ].indexOf(event.keyCode) === -1)
    .map( event => event.target.value )
    .pipe( tap( _ => {

      // Reset stream if input phrase length is less than 3 chars
      if (_.length === 0) {
        this._data = [];
        this._reset$.next();
      }
    }) )
    // Start filtering if input length is more than 3 characters
    .filter( val => val.length > 2 )
    .pipe( tap( _ => this._data = [] ) )
    .distinctUntilChanged()

    // Supply current data state to the filtering pipe
    .pipe( switchMap( _ => this._dataState$, (phrase, doc) => ({ phrase: phrase, doc: doc}) ))

    // Filter by every property value
    .filter( (doc: any, index: number) =>
              Object.values(doc.doc).join('').toString().toLowerCase().indexOf( doc.phrase.toString().toLowerCase()  ) > -1 )
    .map( filtered => filtered.doc )
    .pipe( take(this._page.pageLimit));

    this.updateTable(filter$);
  }

  constructor(
    private dataService: DataService
  ) { }

  ngOnInit() {

    // Retrieve data from the service
    const data$ = this.dataService.getDataObservable();

    const limit$ = data$.pipe( take( this._page.pageLimit ));

    // Reset stream when user clears search input
    const reset$ = this._reset$.pipe( mergeMap( () => limit$ ) );

    const result$ = merge(limit$/*, filter$*/, reset$);

    // Make resusable observable `hot`
    this._dataState$ = data$;

    this._dataSubscription = result$.subscribe(
      d => {
        this._data.push(d);
      },
      err => {},
      () => console.log('Complete!')
    );

  }

  ngOnDestroy() {
    if (this._dataSubscription) {
      this._dataSubscription.unsubscribe();
    }
  }

}
