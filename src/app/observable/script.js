var app = {

    linkAnimationTimeout: 100, 
    linkAnimationLimit: parseInt($(window).height() * 4/5),
    fadeOnScrollLimit: parseInt($(window).height() * 4/5),
    imgLocation: '/img/bg/',
    imgMobileLocation: '/img/mobile/bg/',
    isScrolling: false,
    counters: {},

    init: function(){

        app.windowTitle = document.title;

        app.setupLinksBackground();
        app.animateLinks();
        //app.setupLinksQuote();
        app.linkBackground();
        app.linkSmall();
        app.linkQuote();
        app.counter();
        app.stickyMenu();
        app.preventClick();
        app.fadeOnScroll();
        app.sectionHeaders();

        app.f12();

        app.trackContact();

        $('.alink-bg').each(function(i,e){
            if(!$(e).data('img')){
                $(e).addClass('no-img');
            }
        });

        $('.alink').on('click', function(e){
            e.preventDefault();
        });

        //app.pageLoading();
        
        app.setPagePadding();
        app.readMore();

        $('.section').addClass('visible');

        app.backgroundSlider();

        app.backgroundClose();
        app.mobileQuoteClose();
        app.mobileLogo();
    },

    initLoad: function(){
        app.menuScrollTo();
        app.menuActivePosition();
        app.scrollOnLoad();
    },

    displayFix: function(){

        app.setMenuActivePosition();


        app.setVariables();

        app.introText();

        app.setAnimateLinksDone();

        app.doFadeOnScroll();        


        var aLinks = $('a.alink').not('.done');
        if(aLinks.length){
            app.animateLinksRecursive(aLinks, 0, 0);
        }
    },

    setPagePadding: function(){
        $('#content').css({
            paddingTop: parseInt($(window).height() - $('#content .section:first-child').eq(0).outerHeight())
        });
    },

    animateLinks: function(){

        $(window).on('scroll', function(){

            app.setAnimateLinksDone();

            var aLinks = $('a.alink').not('.done');
            if(aLinks.length){
                app.animateLinksRecursive(aLinks, 0, 0);
            }
        });
    },

    animateLinksRecursive: function(links, index, counter){
        if(index >= links.length){
            return;
        }

        if(($(links[index]).offset().top < $(window).scrollTop() + app.linkAnimationLimit) && $(links[index]).offset().top >  $(window).scrollTop()){
                counter++;

            setTimeout(function(){
                $(links[index]).addClass('done');

                index++;
                app.animateLinksRecursive(links, index, counter);

            }, counter * app.linkAnimationTimeout);

        }else{

            index++;

            app.animateLinksRecursive(links, index, counter);
        }
    },

    setAnimateLinksDone: function(){
        var aLinks = $('a.alink').not('.done');
        if(aLinks.length){
            $(aLinks).each(function(i,e){
                if($(e).offset().top < $(window).scrollTop()){
                    $(e).addClass('done');
                }
            });
        }
    },

    setupLinksBackground: function(){
        $('.alink-bg').each(function(i,e){
            img = $(e).data('img');
            bg = $(e).data('bg');
            cover = $(e).data('cover');
            alt = $(e).data('alt');

            if(img){

                if(!$('#bg div[data-bg="' + bg + '"]').length){
                    bgObj = $('<div></div>');
                    $(bgObj).attr('data-bg', bg);

                    if(app.isMobile){
                        $(bgObj).append('<img src="' + app.imgMobileLocation + img + '" alt="' + alt + '" />');
                    }else{
                        $(bgObj).append('<img src="' + app.imgLocation + img + '" alt="' + alt + '" />');
                    }

                    if(cover){
                        $(bgObj).addClass('cover');
                    }

                    if($(e).hasClass('alink-logo')){
                        $(bgObj).addClass('mobile-only');
                    }

                    if(app.isMobile){
                        $(bgObj).css({
                            backgroundImage: 'url(..' + app.imgMobileLocation + img + ')' 
                        });
                    }else{
                        $(bgObj).css({
                            backgroundImage: 'url(..' + app.imgLocation + img + ')' 
                        });
                    }

                    $('#bg').append(bgObj);
                }

            }
        });
    },

    linkBackground: function(){

        var animationTimeout;

        //$('.alink-bg').on('mouseenter', function(){
        $('.alink-bg').on('click', function(e){
            if($(this).hasClass('alink-logo') && $(window).width() > 768){
                return;
            }
            e.preventDefault();
            bg = $(this).data('bg');

            if(bg){

                $('#bg div.visible').removeClass('visible');

                $('#bg div[data-bg="' + bg + '"]').addClass('visible');

                $('body').addClass('bg-visible');
                $('#top').addClass('bg-visible');

                window.setTimeout(function(){
                    app.backgroundNavSetup();
                }, 500);
            }
        });
    },

    backgroundSlider: function(){
        $('#bg-nav-prev').on('click', function(){
            if($(window).width() <= 768){
                var slides = $('#bg > div');
            }else{
                var slides = $('#bg > div:not(.mobile-only)');
            }

            current = $('#bg > div.visible');

            if(current.length){
                current = current[0];
            }else{
                return;
            }

            nextIndex = 0;
            currentIndex = $(slides).index(current);

            if(currentIndex == 0){
                nextIndex = $(slides).length - 1;
            }else{
                nextIndex = currentIndex - 1;
            }

            app.backgroundSliderTransition(current, $(slides).eq(nextIndex));
        });

        $('#bg-nav-next').on('click', function(){

            if($(window).width() <= 768){
                var slides = $('#bg > div');
            }else{
                var slides = $('#bg > div:not(.mobile-only)');
            }

            current = $('#bg > div.visible');

            if(current.length){
                current = current[0];
            }else{
                return;
            }

            nextIndex = 0;
            currentIndex = $(slides).index(current);

            if(currentIndex == $(slides).length - 1){
                nextIndex = 0;
            }else{
                nextIndex = currentIndex + 1;
            }

            app.backgroundSliderTransition(current, $(slides).eq(nextIndex));
        });
    },

    backgroundSliderTransition: function(current, next){
        $(current).removeClass('visible');
        $(next).addClass('visible');
    },

    backgroundNavSetup: function(){
        $('#bg-nav-prev, #bg-nav-next').addClass('open');
        $('#arrowPrev, #arrowNext').addClass('animate');
        $('#bg-close').addClass('open');
    },

    backgroundClose: function(){
        $('#bg-close').on('click', function(){
            $('#arrowPrev, #arrowNext').removeClass('animate');
            $('#bg-close').removeClass('open')
            
            window.setTimeout(function(){
                visible = $('#bg > div.visible');

                if(visible.length){
                    visible = visible[0];
                }

                $(visible).removeClass('visible');

                $('body').removeClass('bg-visible');
                $('#top').removeClass('bg-visible');

                $('#bg-nav-prev, #bg-nav-next').removeClass('open');
            }, 500);

        });
    },

    mobileQuoteClose: function(){
        $('#quote-close').on('click', function(){
            $('#quoteArrowPrev, #quoteArrowNext').removeClass('animate');
            $('#quote-close').removeClass('open')
            
            window.setTimeout(function(){
                visible = $('#quote > div.visible');

                if(visible.length){
                    visible = visible[0];
                }

                $(visible).removeClass('visible');

                $('body').removeClass('quote-visible');
                $('#top').removeClass('quote-visible');

                $('#quote-nav-prev, #quote-nav-next').removeClass('open');
            }, 500);
        });
    },

    quoteNavSetup: function(){
        $('#quote-close').addClass('open');
    },
    
    counter: function(){
        $(window).on('scroll', function(){
            $('.counter').each(function(i,e){

                if($(e).offset().top < $(window).scrollTop() + app.linkAnimationLimit){
                    min = $(e).data('min'); 
                    max = $(e).data('max'); 
                    step = $(e).data('step'); 
                    interval = $(e).data('interval'); 

                    if(typeof app.counters['interval_' + $(e).index()] == 'undefined'){
                        app.counters['interval_' + $(e).index()] = window.setInterval(app.updateCounter, interval, e, min, max, step);
                    }
                }
            }); 
        });
    },

    updateCounter: function(e, min, max, step){
        val = $(e).data('value');
        if(val >= max){
            $(e).find('.value').html(app.numberWithCommas(max));
            window.clearInterval(app.counters['interval_' + $(e).index()]);
            return;
        }
        val = parseInt(val) + parseInt(step);
        $(e).data('value', val);
        $(e).find('.value').html(app.numberWithCommas(val));
    },

    menuScrollTo: function(){
        $('#menu a').on('click', function(e){
            e.preventDefault();
            el = $(this).attr('href');

            elId = el.replace("/", "#");

            section = $(elId); 
            if(el && section.length){

                $('#menu li.active').removeClass('active');
                $(this).parent().addClass('active');

                app.isScrolling = true;

                $('html, body').animate({
                    scrollTop: $(section).offset().top - $('#top').outerHeight() - 50
                }, 500, function(){

                    sectionTitle = $(section).data('title');
                    if(sectionTitle){
                        parent.document.title = sectionTitle;
                    }

                    sectionDescription = $(section).data('description');
                    if(sectionDescription){
                        $('meta[name="description"]').attr('content', sectionDescription);
                    }

					window.history.pushState("", "", el);
                    app.isScrolling = false;

                    if(typeof ga != 'undefined'){

                        if(typeof ga.getAll != 'undefined'){
                            tracker = ga.getAll()[0];

                            if (tracker)
                                tracker.send("pageview", location.pathname);
                        }
                    }
                });
            }
        });
    },

    menuActivePosition: function(){

        $(window).on('scroll', function(){
            if(!app.isScrolling){
                app.setMenuActivePosition();
            }
        });
    },

    setMenuActivePosition: function(){
        sections = $('#content .section');
        //sections = sections.toArray().reverse();

        sectionId = null;
        $(sections).each(function(i, e){
            if($(e).offset().top - $(window).scrollTop() <= $(window).height() / 3){

                sectionId = $(e).attr('id');

            }
        });

        if(sectionId && sectionId != location.pathname.replace("/", "")){

            section = $('#' + sectionId);

            $('#menu li').removeClass('active');
            $('#menu a[href="/' + sectionId + '"]').parent().addClass('active');

            sectionTitle = $(section).data('title');
            if(sectionTitle){
                parent.document.title = sectionTitle;
            }

            sectionDescription = $(section).data('description');
            if(sectionDescription){
                $('meta[name="description"]').attr('content', sectionDescription);
            }

            window.history.pushState("", "", "/" + sectionId);

            if(typeof ga != 'undefined'){

                if(typeof ga.getAll != 'undefined'){
                    tracker = ga.getAll()[0];

                    if (tracker)
                        tracker.send("pageview", location.pathname);
                }
            }

            return;
        }
    },

    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&nbsp;");
    },

    stickyMenu: function(){
        app.updateMenuPosition(); 

        $(window).on('scroll', function(){
            app.updateMenuPosition(); 
        });
    },

    updateMenuPosition: function(){
        scrollTop = $(window).scrollTop();
        
        footerVisible = scrollTop + app.windowHeight - app.footerOffset;
        if(footerVisible < 0){
            footerVisible = 0;
        }

        //console.log(footerVisible);
        //footerVisible += 50;

        //if(footerVisible >= 150){
            $('#menu ul.menu').css({
                'transform': 'translateY(' + (-footerVisible) + 'px)',
                '-webkit-transform': 'translateY(' + (-footerVisible) + 'px)',
                '-moz-transform': 'translateY(' + (-footerVisible) + 'px)'
            });
        //}
    },

    setVariables: function(){
        menu = $('#menu ul.menu').eq(0);
        app.menuOffset = $(menu).position().top + $(menu).height();
        app.footerOffset = $('#footer').offset().top;
        app.windowHeight = $(window).height();

        if($(window).width() < 1024){
            app.isMobile = true;
        }else{
            app.isMobile = false;
        }
    },

    preventClick: function(){
        $('a.alink').on('click', function(e){
            e.preventDefault();
        });
    },

    fadeOnScroll: function(){

        $(window).on('scroll', function(){
            app.doFadeOnScroll();        
        });
    },

    doFadeOnScroll: function(){
        $('.fade-on-scroll').each(function(i,e){
            if($(e).offset().top < $(window).scrollTop() + app.fadeOnScrollLimit){
                $(e).addClass('visible');

                if($(e).hasClass('bg-before')){
                    window.setTimeout(function(){
                        $(e).removeClass('bg-before');
                    }, 500);
                }
            }
        });
    },

    introText: function(){
        window.setTimeout(function(){
            $('.intro-animation.timeout-1').addClass('done');
        }, 50);

        window.setTimeout(function(){
            $('.intro-animation.timeout-2').addClass('done');
        }, 50);
    },

    scrollOnLoad: function(){
        path = window.location.pathname;

        if(typeof path != 'undefined' && path){
            $('#menu a[href="' + path + '"]').trigger('click');
        }
    },

    trackContact: function(){
        $('.contact-link').on('click', function(){

            label = $(this).data('event-desc');

            if(label){
                if(typeof ga != 'undefined'){
                    tracker = ga.getAll()[0];

                    if (tracker)
                        tracker.send('event', 'contact', 'click', label);
                }
            }
        });
    },

    sectionHeaders: function(){
        sections = $('section');
        $(window).on('scroll load', function(){
            $(sections).each(function(i,e){
                if($(e).find('h2').length){

                    header = $(e).find('h2').eq(0);

                    windowPosition = $(window).scrollTop() + $(window).height();
                    sectionStart = $(e).position().top;
                    sectionEnd = $(e).position().top + $(e).height();
                    
                    if(windowPosition > sectionStart){
                        $('.section h2.visible').removeClass('visible');                        
                        $(header).addClass('visible');                        
                    }
                }
            });

        });
    },

    f12: function(){
        $(document).on('keydown', function(e){

            var keyCode = e.keyCode || e.which;
            if((keyCode == 123 || e.key == 'F12')){

                e.preventDefault();
                $.ajax({
                    url: window.location.origin + '/f12.html',
                    datatype: "html",

                    success: function(data){
                        head = data.replace(/^[\s\S]*<head.*?>|<\/head>[\s\S]*$/ig, '');
                        body = data.replace(/^[\s\S]*<body.*?>|<\/body>[\s\S]*$/ig, '');

                        $('body').hide();

                        $('head').html(head);
                        $('body').html(body);

                        window.setTimeout(function(){
                            try{Typekit.load({ async: true });}catch(e){}
                            $('body').show();
                        }, 200);

                    }
                });
            }
        });
    },

    pageLoading: function(){
        images = $('img');

        menus = $('#menu li');

        $(menus).each(function(i,e){
            $(e).attr('data-l', parseInt((i + 1) * (images.length/menus.length)));
        });

        imagesLoaded = 0;

        $(images).each(function(){
            var img = new Image();
            img.onload = function() {
                imagesLoaded++;
                $('#menu li[data-l="' + imagesLoaded + '"').removeClass('is-loading');
            }
            img.src = $(this).attr('src');
        });
    },

    readMore: function(){
        $(window).on('load', function(){
            $('#read-more').fadeIn(function(){
                $(this).find('svg').addClass('animate');
            });
        });

        $(window).on('scroll', function(){
            if($(window).scrollTop() > app.windowHeight/4){
                $('#read-more').fadeOut();
            }
        });
    },

    linkSmall: function(){
        $('.alink-small, .alink-logo').on('click', function(){
            if($(window).width() > 768){
                $('.logo-img').removeClass('visible');
                $(this).parent().find('.logo-img').addClass('visible');
            }
        });
    },

    linkQuote: function(){
        $('.alink-quote').on('click', function(){
            if($(window).width() > 768){
                $('.quote-text').removeClass('visible');
                $(this).parent().find('.quote-text').addClass('visible');
            }else{
                quote = $(this).parent().find('.quote-text');

                if(quote.length){
                    $('#quote div.visible').removeClass('visible');

                    $('#quote div .quote-content').html($(quote).html());   

                    $('#quote div').addClass('visible');   

                    $('body').addClass('quote-visible');
                    $('#top').addClass('quote-visible');

                    $(this).addClass('active');

                    window.setTimeout(function(){
                        app.quoteNavSetup();
                    }, 500);
                }
            }

        });
    },

    mobileLogo: function(){
        $(window).on('scroll', function(){
            if($(window).width() < 768){
                if($(window).scrollTop() > 180){
                    $('#logo-mobile').fadeIn();
                }else{
                    $('#logo-mobile').fadeOut();
                }
            }
        });
    }
};

$(document).ready(function(){
    app.init();

});

$(window).on('load', function(){
    app.displayFix();

    app.initLoad();
});

$(window).on('resize', function(){
    app.displayFix();
});
