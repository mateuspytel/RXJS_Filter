import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompbyclassComponent } from './compbyclass.component';

describe('CompbyclassComponent', () => {
  let component: CompbyclassComponent;
  let fixture: ComponentFixture<CompbyclassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompbyclassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompbyclassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
