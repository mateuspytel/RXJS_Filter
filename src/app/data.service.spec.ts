import { TestBed, inject, async, fakeAsync } from '@angular/core/testing';

import { DataService, DataFormater } from './data.service';
import { LocalStorageService } from 'ngx-webstorage';

describe('DataService', () => {

  const dataFormater = new DataFormater();
  let dataService: DataService;
  let localStorageService;
  const obj1 = { value: null, date: null, string: null };
  const obj2 = { value: null, date: null, string: null };

  let compare_asc, compare_desc;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataService, LocalStorageService]
    });

    localStorageService = new LocalStorageService();
    dataService = new DataService(localStorageService);
  });

  fit('should test if timestamp is correctly converted into text-date format (e.g. 1527068416 => 23 maj 2018)',
  async(() => {
    const timestamp = new Date().getTime() / 1000;
    const result_date = DataFormater.parseTimestamp(timestamp);
    const reg = new RegExp(/^\d{2}\s[a-zA-Z]+\s\d{4}$/);
    expect(result_date).toEqual(jasmine.any(String));
    expect(reg.test(result_date)).toBeTruthy();
  }) );

  fit('should test if text-date format is correctly converted into timestamp',
   async(() => {
    const timestamp = new Date().getTime() / 1000;
    const result_date = DataFormater.parseTimestamp(timestamp);
    const converted_timestamp = DataFormater.stringToDate(result_date);
    expect(converted_timestamp).toEqual(jasmine.any(Number));
    expect(converted_timestamp).toBeGreaterThan(1000000000);
  }));

  fit('should test if text-date format is correctly converted'
  + 'to timestamp and back (e.g. 1527068416 => 23 maj 2018 => 1527068416)',
   async(() => {
    const timestamp = new Date().getTime();
    const epochtimestamp = timestamp / 1000;
    const result_date = DataFormater.parseTimestamp(epochtimestamp);
    const converted_timestamp = DataFormater.stringToDate(result_date);
    expect(converted_timestamp - timestamp).toBeLessThan(1000);
  }));

  fit('should return 0 if input is corrupted or null', async(() => {
    const converted_timestamp = DataFormater.stringToDate(null);
    expect(converted_timestamp).toEqual(0);
  }));

  fit('should correctly compare two numbers (asc and desc)', (async() => {
    obj1.value = 1;
    obj2.value = 2;
    compare_asc = dataService.compareByPropertyName('value', 1);
    expect(compare_asc(obj1, obj2)).toEqual(-1);
    compare_desc = dataService.compareByPropertyName('value', -1);
    expect(compare_desc(obj1, obj2)).toEqual(1);
  }));

  fit('should correctly compare two strings (asc and desc)', async(() => {
    obj1.string = 'aaaaa';
    obj2.string = 'xczcdc';
    compare_asc = dataService.compareByPropertyName('string', 1);
    expect(compare_asc(obj1, obj2)).toEqual(-1);
    compare_desc = dataService.compareByPropertyName('string', -1);
    expect(compare_desc(obj1, obj2)).toEqual(1);
  }));

  fit('should correctly compare two dates (asc and desc)', async(() => {
    obj1.date = '13 mar 2017';
    obj2.date = '26 mar 2017';
    compare_asc = dataService.compareByPropertyName('date', 1);
    compare_desc = dataService.compareByPropertyName('date', -1);
    expect(compare_asc(obj1, obj2)).toEqual(-1);
    expect(compare_desc(obj1, obj2)).toEqual(1);
  }));

  fit('should test if two dates are the same', async(() => {
    obj1.date = '13 mar 2017';
    obj2.date = '13 mar 2017';
    compare_asc = dataService.compareByPropertyName('date', 1);
    compare_desc = dataService.compareByPropertyName('date', -1);
    expect(compare_asc(obj1, obj2)).toEqual(0);
    expect(compare_desc(obj1, obj2)).toEqual(0);
  }));

  fit('should correctly sort number array ASC', fakeAsync(() => {
    obj1.value = 10;
    obj2.value = 20;
    let array = [obj1, obj2];
    array = array.sort(dataService.compareByPropertyName('value', 1));
    expect(array[0].value).toEqual(10);
    expect(array[1].value).toEqual(20);
  }));

  fit('should correctly sort number array DESC', fakeAsync(() => {
    obj1.value = 10;
    obj2.value = 20;
    let array = [obj1, obj2];
    array = array.sort(dataService.compareByPropertyName('value', -1));
    expect(array[0].value).toEqual(20);
    expect(array[1].value).toEqual(10);
  }));

  fit('should correctly sort dates array ASC', async(() => {
    obj1.date = '13 lut 2017';
    obj2.date = '16 mar 2018';

    let array = [obj1, obj2];
    array = array.sort(dataService.compareByPropertyName('date', 1));
    expect(array[0].date).toEqual('13 lut 2017');
    expect(array[1].date).toEqual('16 mar 2018');
  }));

  fit('should correctly sort dates array DESC', async(() => {
    obj1.date = '13 lut 2017';
    obj2.date = '16 mar 2018';

    let array = [obj1, obj2];
    array = array.sort(dataService.compareByPropertyName('date', -1));
    expect(array[0].date).toEqual('16 mar 2018');
    expect(array[1].date).toEqual('13 lut 2017');
  }));

  fit('should correctly sort string array ASC', async(() => {
    obj1.string = 'abc';
    obj2.string = 'zzz';
    const obj3 = { string: 'azd' };
    const obj4 = { string: 'bgh' };
    let array = [obj1, obj2, obj3, obj4];
    array = array.sort(dataService.compareByPropertyName('string', 1));
    expect(array[0].string).toEqual('abc');
    expect(array[1].string).toEqual('azd');
    expect(array[2].string).toEqual('bgh');
    expect(array[3].string).toEqual('zzz');
  }));

  fit('should correctly sort string array DESC', async(() => {
    obj1.string = 'abc';
    obj2.string = 'zzz';
    const obj3 = { string: 'azd' };
    const obj4 = { string: 'bgh' };
    let array = [obj1, obj2, obj3, obj4];
    array = array.sort(dataService.compareByPropertyName('string', -1));
    expect(array[3].string).toEqual('abc');
    expect(array[2].string).toEqual('azd');
    expect(array[1].string).toEqual('bgh');
    expect(array[0].string).toEqual('zzz');
  }));

});
