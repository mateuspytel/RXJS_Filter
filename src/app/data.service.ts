import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { LocalStorageService } from 'ngx-webstorage';
import 'rxjs/add/observable/from';

export interface Data {
  id: number;
  seed: string;
}

const months = ['sty', 'lut', 'mar', 'kwi', 'maj', 'cze', 'lip', 'sie', 'wrz', 'paź', 'lis', 'gru'];

export class DataFormater {

  /**
   * Parses raw timestamp to well formated date string
   * @param {string} timestamp Timestamp in seconds
   */
  static parseTimestamp(timestamp: number): string {
      if (!timestamp) { return; }
      const date = new Date(timestamp * 1000);
      return ('0' + date.getDate()).substr(-2) + ' ' + months[date.getMonth()] + ' ' + date.getFullYear();
  }

  /**
   * Modifies document in array by supplied modifier function
   * @param {any[]} data Data array
   * @param {Function} modifier Modifier function
   */
  static formatInputData(data: any[], modifier: any): Promise<any> {
      return new Promise((resolve, reject) => {
          const output = [];
          data.forEach( document => {
              output.push(modifier(document));
          });
          resolve(output);
      });
  }

  /**
   * Parses date string back to timestamp
   * @param {} str Date string
   */
  static stringToDate(str: string) {
      if (str) {
          const datePart = str.split(' ');
          const date = new Date(Number(datePart[2]), months.indexOf(datePart[1]), Number(datePart[0]));
          return date.getTime();
      }
      return 0;
  }
}

@Injectable()
export class DataService {

  /**
   * Compares to objects by property name
   * @param {string} property Property name
   * @param {number} order Sort order : 1 ASCENDING, -1 DESCENDING
   */
  compareByPropertyName(property: string, order: number) {
    return function (a, b) {

      // Check if sort value is a date format - if so convert to timestamp and compare
      const date = /([0-9]{2})\s([a-z]{1,4})\s([0-9]{4})/i.test(a[property]);
      const number = !isNaN(a[property]);
      let result;

      // Before filtering check if compared string are date / number / string
      if (date || a[property] === null) {
        result = (DataFormater.stringToDate(a[property]) < DataFormater.stringToDate(b[property])) ? -1 :
         ((DataFormater.stringToDate(a[property]) > DataFormater.stringToDate(b[property])) ? 1 : 0);
      } else if (number) {
        result = Number(a[property]) < Number(b[property]) ? -1 : (Number(a[property]) > Number(b[property]) ? 1 : 0 );
      } else {
        result = a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;
      }

      return result === 0 ? 0 : result * order;
    };
  }

  generateData(quantity: number): Array<any> {
    const arr = [];

    for (let i = 0; i < quantity; i++ ) {
      const obj: Data = {
        id: Math.floor(Math.random() * 10000000),
        seed: Math.random().toString(36).substring(7)
      };

      arr.push(obj);
    }

    return arr;
  }


  getDataObservable(): Observable<any> {
    if (!this.localStorage.retrieve('DATA')) {
      console.warn('Generating and saving data to local...');
      const mock = this.generateData(150000);
      this.localStorage.store('DATA', mock);
      return Observable.from(mock);
    } else {
      console.warn('Reading data set from local storage');
      return Observable.from(this.localStorage.retrieve('DATA'));
    }
  }



  getData(quantity: number): Observable<Data> {
    return Observable.create( observer => {

      for (let i = 0; i < quantity; i++ ) {
        const obj: Data = {
          id: Math.floor(Math.random() * 10000000),
          seed: Math.random().toString(36).substring(7)
        };

        observer.next(obj);
      }

      observer.complete();
    });
  }

  constructor(
    private localStorage: LocalStorageService
  ) { }

}
