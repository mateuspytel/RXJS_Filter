import { Injectable } from '@angular/core';

@Injectable()
export class TestService {

  fetchRandomData(quantity: number): Promise<any> {
    return new Promise((resolve, reject) => {
      const array = [];
      if (quantity < 0) { reject('Quantity must be positive number'); }
      for (let i = 0; i < quantity; i++) {
        array.push( {
          id: i,
          date: new Date().toISOString()
        });
      }

      resolve(array);
    });
  }

  constructor() { }

}
