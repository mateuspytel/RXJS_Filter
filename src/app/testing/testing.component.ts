import { Component, OnInit } from '@angular/core';
import { TestService } from './test.service';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.css']
})
export class TestingComponent implements OnInit {

  _data: Array<any>;

  constructor(
    private dataService: TestService
  ) { }

  ngOnInit() {
    this.dataService.fetchRandomData(10)
    .then( data => this._data = data )
    .catch( err => console.error(err) );
  }

}
