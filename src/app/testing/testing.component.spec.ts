import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { TestingComponent } from './testing.component';
import { TestService } from './test.service';

describe('TestingComponent', () => {
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestingComponent ],
      providers: [TestService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  fit('should create', () => {
    expect(component).toBeTruthy();
  });

  fit('should initialize data array', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then( () => {
      fixture.detectChanges();
      expect(component._data).toBeDefined();
    });
    component.ngOnInit();
  }));

  fit('should render data as a table list', fakeAsync(() => {
    fixture.detectChanges();
    fixture.whenStable().then( () => {
      fixture.detectChanges();
      const lis = fixture.nativeElement.querySelectorAll('tr');
      expect(lis.length).toEqual(11);
    });

    component.ngOnInit();
  }));

});
