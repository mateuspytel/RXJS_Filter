import { TestBed, inject, async, fakeAsync } from '@angular/core/testing';

import { TestService } from './test.service';

describe('TestService', () => {

  let testService: TestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TestService]
    });

    testService = TestBed.get(TestService);
  });

  fit('should be created', inject([TestService], (service: TestService) => {
    expect(service).toBeTruthy();
  }));

  fit('should generate data', fakeAsync(() => {
    testService.fetchRandomData(10)
    .then( response => {
      expect(response.length).toEqual(10);
    });
  }));

  fit('should generate data with ISO date format', fakeAsync(() => {
    testService.fetchRandomData(1)
    .then( (data: {id, date} ) => {
      console.log(data);
      const reg = new RegExp(/[0-9]{4}-[0-9]{2}-[0-9]{2}/i);
      expect(reg.test(data[0].date)).toBeTruthy();
    });
  }));

  fit('should reject promise if quantity parameter is negative', fakeAsync( () => {
    testService.fetchRandomData(-1)
    .then( data => {} )
    .catch( error => expect(error).toBe('Quantity must be positive number'));
  }));

});
